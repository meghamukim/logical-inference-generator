#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <utility>
#include <queue>
#include <numeric>
#include <sstream>
using namespace std;

class sentenceLogic{
public:
	map<string,vector<string> > premise;
	map<string,vector<string> > conclusion;
};
bool checkForNoX(vector<sentenceLogic> facts,vector<sentenceLogic> KB,sentenceLogic goalObj);
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

sentenceLogic splitClauses(string sentence,int pos){
	sentenceLogic sentenceObj;
	vector<string> sentencesRHS;
	int i=0;
	string substringRHS;
	string substringLHS;
	
		if(pos != string::npos){//if implication sentence then this condition

			//parse the RHS of the string i.e. after implication
			substringRHS = sentence.substr(pos+2);
			int subPos = substringRHS.find("(");
			if(subPos!= string::npos){
				map<string,vector<string> > RHS;
				string key = substringRHS.substr(0,subPos);
				vector<string> value;
				int subSubPos;
				subSubPos = substringRHS.find(")");
				value.push_back(substringRHS.substr(subPos+1,subSubPos-subPos-1));
				RHS[key] = value;
				sentenceObj.conclusion = RHS;
			}

			//parse the LHS of the string i.e. the conjuncts
			substringLHS = sentence.substr(0,pos);
			map<string,vector<string> > LHS;
			vector<string> splitConjuncts = split(substringLHS,'&');
			for(int j=0;j < splitConjuncts.size();j++){
				int subPos = splitConjuncts[j].find("(");
				if(subPos!= string::npos){
					string key = splitConjuncts[j].substr(0,subPos);
					vector<string> value;
					int subSubPos;
					subSubPos = splitConjuncts[j].find(")");
					value.push_back(splitConjuncts[j].substr(subPos+1,subSubPos-subPos-1));
					LHS[key] = value;
				}
			}
			sentenceObj.premise = LHS;
			return sentenceObj;
		} else{
			//if an atomic sentence then this condition
			int subPos = sentence.find("(");
			if(subPos!= string::npos){
				map<string,vector<string> > LHS;
				vector<string> valueLHS;
				valueLHS.push_back("");
				LHS[""] = valueLHS;	
				sentenceObj.premise = LHS;

				map<string,vector<string> > RHS;
				vector<string> valueRHS;
				string key = sentence.substr(0,subPos);
				int subSubPos;
				subSubPos = sentence.find(")");
				valueRHS.push_back(sentence.substr(subPos+1,subSubPos-subPos-1));
				RHS[key] = valueRHS;
				sentenceObj.conclusion = RHS;
			}
			return sentenceObj;
		}
}

bool matchWithFacts(vector<sentenceLogic> facts, sentenceLogic goalObj){
	for(int i=0;i<facts.size();i++){
		if(facts[i].conclusion.begin()->first == goalObj.conclusion.begin()->first){
			vector<string> argsFact = split(facts[i].conclusion.begin()->second[0],',');
			vector<string> argsGoal = split(goalObj.conclusion.begin()->second[0],',');
			if(argsFact.size() == argsGoal.size()){
				if(argsFact[0] == argsGoal[0]){
					if(argsFact.size() != 1){
						if(argsFact[1] == argsGoal[1]){
							return true;
						}
					} else {
						return true;
					}
				}
			}
		}
	}
	return false;
}

sentenceLogic createGoal(vector<string> argsPremise,string key){
//create next goal
	sentenceLogic goalNext;
	map<string,vector<string> > LHS;
	vector<string> valueLHS;
	valueLHS.push_back("");
	LHS[""] = valueLHS;	
	goalNext.premise = LHS;

	map<string,vector<string> > RHS;
	vector<string> valueRHS;
	if(argsPremise.size() != 1)
		valueRHS.push_back(argsPremise[0]+","+argsPremise[1]);
	else
		valueRHS.push_back(argsPremise[0]);
	RHS[key] = valueRHS;
	goalNext.conclusion = RHS;
	return goalNext;
}

bool backtrackingAlgorithm(vector<sentenceLogic> facts,vector<sentenceLogic> KB,sentenceLogic goalObj){
	bool isFact = matchWithFacts(facts,goalObj);
	if(isFact){
		return true;
	} else {
			
			for(int i=0;i<KB.size();i++){
				if(KB[i].conclusion.begin()->first == goalObj.conclusion.begin()->first){
					vector<string> argsKB = split(KB[i].conclusion.begin()->second[0],',');
					vector<string> argsGoal = split(goalObj.conclusion.begin()->second[0],',');
					string subst;
					if(argsKB.size() == argsGoal.size()){
						if(argsKB.size() > 1){
							vector<sentenceLogic> globalStackPremise;
							if(argsKB[0] == "x"){
								if(argsKB[1] == argsGoal[1]){
									subst = argsGoal[0];
									map<string,vector<string> > :: iterator it;
									for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
										vector<string> argsPremise = split(it->second[0],',');
										if(argsPremise.size() > 1){
											if(argsPremise[1] == "x"){
												argsPremise[1] = subst;
											}
										}
										if(argsPremise[0] == "x"){
											argsPremise[0] = subst;
										}
										
										sentenceLogic goalNext = createGoal(argsPremise,it->first);
										globalStackPremise.push_back(goalNext);
									}
								} else{
									continue;
								}
							} else if(argsKB[1] == "x"){
								if(argsKB[0] == argsGoal[0]){
									subst = argsGoal[1];
									map<string,vector<string> > :: iterator it;
									for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
										vector<string> argsPremise = split(it->second[0],',');
										if(argsPremise.size() > 1){
											if(argsPremise[1] == "x"){
												argsPremise[1] = subst;
											}
										} 
										if(argsPremise[0] == "x"){
											argsPremise[0] = subst;
										}
										sentenceLogic goalNext = createGoal(argsPremise,it->first);
										globalStackPremise.push_back(goalNext);
									}
								} else {
									continue;
								}
							} else if(argsKB[0]!="x" && argsKB[1]!="x"){

								if(argsKB.size() == argsGoal.size()){
									if(argsGoal.size()>1){
										if(argsGoal[0] == "x" || argsGoal[1] == "x"){
											if(argsGoal[0]==argsKB[0] || argsGoal[1]==argsKB[1]){
												vector<bool> isValid = vector<bool>(KB[i].premise.size());
												int j=0;
												map<string,vector<string> > :: iterator it;
												for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
													vector<string> argsPremise = split(it->second[0],',');
													sentenceLogic goalNext = createGoal(argsPremise,it->first);
										
													isValid[j++] = checkForNoX(facts,KB,goalNext);
												}
												for(int k=0;k<j;k++){
													if(isValid[k] == false){
														return false;
													}
												}
											} else {
												continue;
											}
										} else if(argsGoal[0]==argsKB[0] && argsGoal[1]==argsKB[1]){
											map<string,vector<string> > :: iterator it;
											vector<bool> isValid = vector<bool>(KB[i].premise.size());
											int j=0;
											for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
												vector<string> argsPremise = split(it->second[0],',');
												sentenceLogic goalNext = createGoal(argsPremise,it->first);
										
												isValid[j++] = checkForNoX(facts,KB,goalNext);
											}
											for(int k=0;k<j;k++){
												if(isValid[k] == false){
													return false;
												}
											}
										} else {
											return false;
										}
									} else if(argsGoal[0] == argsKB[0] || argsGoal[0] == "x"){

										map<string,vector<string> > :: iterator it;
										vector<bool> isValid = vector<bool>(KB[i].premise.size());
										int j=0;	
										for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
											vector<string> argsPremise = split(it->second[0],',');
											sentenceLogic goalNext = createGoal(argsPremise,it->first);
										
											isValid[j++] = checkForNoX(facts,KB,goalNext);
										}
										for(int k=0;k<j;k++){
										if(isValid[k] == false){
											return false;
										}
									}
									} else {
										return false;
									}
								} else {
									return false;
								}
								return true;
							}
							while(!globalStackPremise.empty()){
								bool isQueryValid = backtrackingAlgorithm(facts,KB,globalStackPremise[globalStackPremise.size()-1]);
								if(isQueryValid){
									globalStackPremise.pop_back();
								} else {
									return false;
								}
							}
							if(globalStackPremise.empty()){
								return true;
							} else {
								return false;
							}
						} else if(argsKB[0] == "x"){
							vector<sentenceLogic> globalStackPremise;
							subst = argsGoal[0];
							map<string,vector<string> > :: iterator it;
							for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
								vector<string> argsPremise = split(it->second[0],',');
								if(argsPremise.size() > 1){
									if(argsPremise[1] == "x"){
										argsPremise[1] = subst;
									}
								} 
								if(argsPremise[0] == "x"){
									argsPremise[0] = subst;
								}
								sentenceLogic goalNext = createGoal(argsPremise,it->first);
								globalStackPremise.push_back(goalNext);
							}
							while(!globalStackPremise.empty()){
								bool isQueryValid = backtrackingAlgorithm(facts,KB,globalStackPremise[globalStackPremise.size()-1]);
								if(isQueryValid){
									globalStackPremise.pop_back();
								} else {
									return false;
								}
							}
							if(globalStackPremise.empty()){
								return true;
							} else {
								return false;
							}
						} else if(argsKB[0]!="x"){
							if(argsKB.size() == argsGoal.size()){
								vector<bool> isValid = vector<bool>(KB[i].premise.size());
								int j=0;
								map<string,vector<string> > :: iterator it;
								for(it=KB[i].premise.begin(); it!= KB[i].premise.end(); ++it){
									vector<string> argsPremise = split(it->second[0],',');
									sentenceLogic goalNext = createGoal(argsPremise,it->first);
									isValid[j++] = checkForNoX(facts,KB,goalNext);
								}
								for(int k=0;k<j;k++){
									if(isValid[k] == false){
										return false;
									}
								}
							} else {
								return false;
							}
							return true;
						} else {
								continue;
							}
						} else {
							continue;
						}
					}
				}
			}
	return false;
}

bool checkForNoX(vector<sentenceLogic> facts,vector<sentenceLogic> KB,sentenceLogic goalObj){
	
	for(int i=0;i<facts.size();i++){
		if(facts[i].conclusion.begin()->first == goalObj.conclusion.begin()->first){
			vector<string> argsFact = split(facts[i].conclusion.begin()->second[0],',');
			vector<string> argsGoal = split(goalObj.conclusion.begin()->second[0],',');
			if(argsFact.size() == argsGoal.size()){
				if(argsGoal.size() > 1){
					if(argsGoal[0] == "x"){
						if(argsFact[1] == argsGoal[1]){
							return true;
						} else {
							return false;
						}
					} else if(argsGoal[1] == "x"){
						if(argsFact[0] == argsGoal[0]){
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else if(argsGoal[0] == "x"){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	bool valToReturn = backtrackingAlgorithm(facts,KB,goalObj);
	return valToReturn;
}
int main(){
	string number;
	vector<string> sentences;
	int noOfClauses;
	string goalQuery;

	ifstream ifile;
	fstream ofile;
	ifile.open("input.txt", ios::in);

	getline(ifile,goalQuery);

	getline(ifile,number);
	noOfClauses = atoi(number.c_str());
	
	string line;
	while(getline(ifile,line)){
		sentences.push_back(line);
	}
	
	ifile.close();
	vector<sentenceLogic> KB;
	vector<sentenceLogic> facts;
	int pos;

	for(int i=0;i < sentences.size();i++){
		pos= sentences[i].find("=>");
		if(pos != string::npos){//if implication sentence then this condition
			KB.push_back(splitClauses(sentences[i],pos));
		} else {
			facts.push_back(splitClauses(sentences[i],pos));
		}
	}

	sentenceLogic goalObj = splitClauses(goalQuery,string::npos);

	bool isQueryTrue = backtrackingAlgorithm(facts,KB,goalObj);
	ofile.open("output.txt",ios::out);
	if(isQueryTrue){
		ofile << "TRUE";
	} else {
		ofile << "FALSE";
	}
	ofile.close();
	return 0;
}
